use ndarray::{array, Array1, Array2, ArrayView2};
/// This neural network makes a 
fn neural_network(input : Array1<f32>, weights : [ArrayView2<f32>; 2] ) -> Array1<f32> {
    let hidden = input.dot(&weights[0]);
    hidden.dot(&weights[1])
}

pub fn make_prediction() -> Array1<f32> {
    let input_to_hidden_weights : Array2<f32> = array![
        [0.1, 0.2, -0.1], // hid[0]
        [-0.1, 0.1, 0.9], // hid[1]
        [0.0, 0.4, 0.1],  // hid[2]
    ];
    
    let hidden_to_prediction_weights : Array2<f32> = array![
        [0.3, 1.1, -0.3], // hurt?
        [0.1, 0.2, 0.0], // win?
        [0.0, 1.3, 0.1], // sad?   
    ];

    let weights : [ArrayView2<f32>; 2] = [input_to_hidden_weights.t(), hidden_to_prediction_weights.t()];
    
    let toes = 8.5;
    let win_ratio = 0.65;
    let millions_of_fans = 1.2;

    let input : Array1<f32> = array![toes, win_ratio, millions_of_fans];

    neural_network(input, weights)
}

pub fn print_prediction() {
    let prediction = make_prediction();
    if let Some(hurt_pct) = prediction.get(0) {
        println!("hurt: {}", hurt_pct);
    }

    if let Some(win_pct) = prediction.get(1) {
        println!("win: {}", win_pct);
    }

    if let Some(sad_pct) = prediction.get(2) {
        println!("sad: {}", sad_pct);
    }
}