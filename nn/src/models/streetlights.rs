use ndarray::{array, Array, Array2, ArrayView2, s};
use ndarray_rand::RandomExt;
use ndarray_rand::rand::SeedableRng;
use ndarray_rand::rand_distr::Uniform;
use rand_isaac::isaac64::Isaac64Rng;

pub fn relu2derivative(input: &Array2<f64>) -> Array2<f64> {
    input.map(|v| if *v < 0.0 { 0.0 } else { 1.0 })
}

pub fn relu(input: &Array2<f64>) -> Array2<f64> {
    input.mapv(|v| if v < 0.0 { 0.0 } else { v })
}

pub fn print_prediction() {
    let alpha = 0.052;
    let hidden_size = 4;

    let streetlights : Array2<f64> = array![
            [1.0, 0.0, 1.0],
            [0.0, 1.0, 1.0],
            [0.0, 0.0, 1.0],
            [1.0, 1.0, 1.0],
    ];

    let walk_vs_stop : Array2<f64> = array![[1.0, 1.0, 0.0, 0.0]];
    let walk_vs_stop = walk_vs_stop.t();
    let seed = 52;
    let mut rng = Isaac64Rng::seed_from_u64(seed);
    let mut weights_0_1 : Array2<f64> = 2.0 * Array::random_using((3, hidden_size), Uniform::new(0.0, 1.0), &mut rng) - 1.0;
    let mut weights_1_2 : Array2<f64> = 2.0 * Array::random_using((hidden_size, 1), Uniform::new(0.0, 1.0), &mut rng) - 1.0;
    // let mut weights_0_1 : Array2<f64> = Array::from_elem((3,4), 0.5);
    // let mut weights_1_2 : Array2<f64> = Array::from_elem((4,1), 0.5);

    for iteration in 1..60000 {
        let mut layer_2_error = 0.0;
        for i in 0..streetlights.nrows() {
            let layer_0 : ArrayView2<f64> = streetlights.slice(s![i..i+1, ..]);
            let layer_1 : Array2<f64> = relu(&layer_0.dot(&weights_0_1));
            let layer_2 : Array2<f64> = layer_1.dot(&weights_1_2);
            let current_result_slice : ArrayView2<f64> = walk_vs_stop.slice(s![i..i+1, ..]);
            layer_2_error = layer_2_error + (&layer_2 - &current_result_slice).mapv(|v| v * v).sum();
            let layer_2_delta = &current_result_slice - &layer_2;
            let layer_1_delta = layer_2_delta.dot(&weights_1_2.t()) * relu2derivative(&layer_1);
            weights_1_2 = weights_1_2 + &(layer_1.t().dot(&layer_2_delta).mapv(|v| v * alpha));
            weights_0_1 = weights_0_1 + &(layer_0.t().dot(&layer_1_delta).mapv(|v| v * alpha));
            // println!("layer_0: {:?}", layer_0);
            // println!("layer_1: {:?}", layer_1);
            // println!("layer_2: {:?}", layer_2);
            // println!("current_result_slice: {:?}", current_result_slice);
            // println!("layer_2_error: {}", layer_2_error);
            // println!("layer_2_delta: {}", layer_2_delta);
            // println!("layer_1_delta: {}", layer_1_delta);
            // println!("weights_1_2: {:?}", weights_1_2);
            // println!("weights_0_1: {:?}", weights_0_1);
        }
        if iteration % 10 == 9 {
            println!("Error: {}", layer_2_error);
        }
    }
}
